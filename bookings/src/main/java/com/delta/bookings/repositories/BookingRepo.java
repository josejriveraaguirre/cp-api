package com.delta.bookings.repositories;

import com.delta.bookings.models.Booking;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface BookingRepo extends JpaRepository<Booking, Long> {

        // the following method is equivalent to the built-in 'getOne' method, just to demonstrate that we can do it here to.

        @Query("from Booking booking where booking.id like ?1")
        Booking getBookingById(long id);
    
}

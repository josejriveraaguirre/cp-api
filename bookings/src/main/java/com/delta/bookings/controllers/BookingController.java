package com.delta.bookings.controllers;

import com.delta.bookings.models.Booking;
import com.delta.bookings.repositories.BookingRepo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


import java.util.List;

@Controller
public class BookingController {

    // spring boot's dependency injection

    private final BookingRepo bookingRepository;

    public BookingController(BookingRepo bookingRepository) {
        this.bookingRepository = bookingRepository;
    }

    // gets all the bookings and displays them in "bookings/index" view.

    @GetMapping("/bookings")
    public String showBookings(Model model) {

        List<Booking> bookingList = bookingRepository.findAll();
        model.addAttribute("noBookingsFound", bookingList.size() == 0);
        model.addAttribute("bookings", bookingList);
        return "bookings/index"; // name of the view

    }

    // show the create/add an ad view

    @GetMapping("/bookings/create")
        public String showForm(Model model) {
        model.addAttribute("newBooking", new Booking());
        return "bookings/create";
    }

    @PostMapping("/bookings/create")
    public String createBooking(@ModelAttribute Booking bookingToCreate) {
        // save the bookingToCreate parameter
        bookingRepository.save(bookingToCreate);
        // redirect the user to the list of bookings -> /bookings
        return "redirect:/bookings/";
    }

    // controller method to display an individual booking
    @GetMapping("/bookings/{id}")
    public String showBooking(@PathVariable long id, Model model) {
        Booking booking = bookingRepository.getById(id);
        model.addAttribute("showBooking", booking);
        return "/bookings/show";
    }

    // controller method that will allow for our user to edit
    @GetMapping("bookings/{id}/edit")
    public String showEdit(@PathVariable long id, Model model) {
        // find the booking object with the passed in id
        Booking bookingToEdit = bookingRepository.getById(id);
        model.addAttribute("editBooking", bookingToEdit);
        return "bookings/edit";
    }

    @PostMapping("/bookings/{id}/edit")
    public String updateBooking(@PathVariable long id,
                           @RequestParam(name ="firstName") String firstName,
                           @RequestParam(name = "lastName") String lastName) {

        // find the booking with the passed in id
        Booking foundBooking = bookingRepository.getBookingById(id); //NOTE in mySQL -> SELECT * FROM bookings WHERE id = ?

        // update the booking's first name and last name
        foundBooking.setGuestFirstName(firstName);
        foundBooking.setGuestLastName(lastName);

        // save the new booking's data changes
        bookingRepository.save(foundBooking);

        // redirect user to the url that contains the list of bookings
        return "redirect:/bookings/";
    }

    // controller method to delete objects of our class
    @PostMapping("/bookings/{id}/delete")
    public String delete(@PathVariable long id) {
        // access the deleteById() method from the repository
        bookingRepository.deleteById(id);

        // redirect user to url for list of ads
        return "redirect:/bookings/";
    }

} // end of class

// import java.util.List;

// import com.delta.bookings.models.Booking;
// import com.delta.bookings.services.BookingService;

// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.http.HttpStatus;
// import org.springframework.http.ResponseEntity;
// import org.springframework.web.bind.annotation.CrossOrigin;
// import org.springframework.web.bind.annotation.DeleteMapping;
// import org.springframework.web.bind.annotation.GetMapping;
// import org.springframework.web.bind.annotation.PathVariable;
// import org.springframework.web.bind.annotation.PostMapping;
// import org.springframework.web.bind.annotation.RequestBody;
// import org.springframework.web.bind.annotation.RequestMapping;
// import org.springframework.web.bind.annotation.RestController;

// @CrossOrigin("*")
// @RestController
// @RequestMapping("/api/v1")
// public class BookingController {

//     @Autowired
//     BookingService bService;

//     @GetMapping("/bookings")
//     public ResponseEntity<List<Booking>> get() {
//         List<Booking> bookings = bService.findAll();
//         return new ResponseEntity<List<Booking>>(bookings, HttpStatus.OK);
//     }

//     @PostMapping("/bookings/create")
//     public ResponseEntity<Booking> save(@RequestBody Booking booking) {
//         Booking newBooking = bService.save(booking);
//         return new ResponseEntity<Booking>(newBooking, HttpStatus.OK);
//     }
    
//     @GetMapping("/bookings/edit/{id}")
//     public ResponseEntity<Booking> getBooking(@PathVariable("id") Long id) {
//         Booking viewBooking = bService.findById(id);
//         return new ResponseEntity<Booking>(viewBooking, HttpStatus.OK);
//     }

//     @DeleteMapping("/bookings/edit/{id}")
//     public ResponseEntity<String> delete(@PathVariable("id") Long id) {
//         bService.delete(id);
//         return new ResponseEntity<String>("Booking was deleted successfully.", HttpStatus.OK);
//     }
            
// } // end of class



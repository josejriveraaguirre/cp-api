package com.delta.bookings.services;

import java.util.List;

import com.delta.bookings.models.Booking;

public interface BookingService {

    List<Booking> findAll();

    Booking save(Booking booking);

    Booking findById(Long id);

    void delete(Long id);
    
}
